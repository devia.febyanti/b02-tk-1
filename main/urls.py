from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('rumah-sakit-rujukan', views.rs_rujukan, name='rumah-sakit-rujukan'),
    path('pencegahan-covid-19', views.pencegahan, name='pencegahan-covid-19'),
    path('test', views.covidtest, name='test'),
    path('result', views.hasiltest, name='result'),
    path('add_comment', views.add_comment, name='add_comment'),
    path('delete/<int:nomor>', views.deletecomment, name='delete'),
    path('pasien', views.get_pasien, name='pasien'),
    path('tambah-pasien', views.add_pasien.as_view(), name='tambah-pasien'),
    path('hapus-pasien', views.delete_pasien.as_view(), name='hapus-pasien'),
    path('pasien/<str:name>', views.get_daftar_pasien, name='data-pasien')
]