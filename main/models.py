from django.db import models

# Create your models here.
class jawabanTest(models.Model):
	question1 = models.BooleanField()
	question2 = models.BooleanField()
	question3 = models.BooleanField()

class Comment(models.Model):
	email = models.EmailField(null=True)
	nama = models.CharField(max_length=10000)
	komentar = models.TextField()
	created = models.DateTimeField(auto_now_add=True)

class Pasien(models.Model):
	nama_pasien = models.CharField(max_length=50, primary_key=True, default=None)
	umur = models.IntegerField(default=None)
	gender = models.CharField(max_length=10, choices=(('Perempuan', 'P',),('Laki-laki', 'L',),), default = None)
	pekerjaan = models.CharField(max_length=30, default=None)
	alamat = models.TextField(blank=True, default=None)
	berat_badan = models.IntegerField(default=None)
	tinggi_badan = models.IntegerField(default=None)
	keluhan = models.TextField(blank=True, default=None)
	email = models.CharField(max_length=50, default=None)