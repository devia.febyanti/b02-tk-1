from django import forms
from .models import jawabanTest, Comment, Pasien
from crispy_forms.helper import FormHelper

class CreateCovidTest(forms.Form):
	question1 = forms.BooleanField(label = 'Apakah kamu pernah melakukan tes usap tenggorok atau hidung dengan RT-PCR hasilnya positif?', required=False)
	question2 = forms.BooleanField(label = 'Apakah ada gejala ISPA seperti demam >38C, batuk pilek, hidung tersumbat, nyeri menelan, atau sesak nafas?', required=False)
	question3 = forms.BooleanField(label = 'Apakah kamu tinggal di wilayah transmisi lokal atau baru saja pulang berpergian ke negara yang melaporkan transmisi lokal dalam 14hr terakhir?', required=False)

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = '__all__'

class tambah_pasien(forms.ModelForm):
    class Meta:
        model = Pasien
        fields = '__all__'

class hapus_pasien(forms.Form):
    nama = forms.ChoiceField(choices=(), required=True, label="Nama Pasien")
    def __init__(self, nama, *args, **kwargs):
        super(hapus_pasien, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields["nama"].choices = nama
